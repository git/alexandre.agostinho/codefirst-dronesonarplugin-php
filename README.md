# codefirst-dronesonarplugin-php
This Docker image should be used for sonarqube integration in PHP projects.

### Usage
```yml
  - name: code-inspection
    image: hub.codefirst.iut.uca.fr/alexandre.agostinho/codefirst-dronesonarplugin-php
    secrets: [ SECRET_SONAR_LOGIN ]
    environment:
      sonar_host: https://codefirst.iut.uca.fr/sonar/
      sonar_token:
        from_secret: SECRET_SONAR_LOGIN
      project_key: << your:project:key >>
      source_folder: << your/source/folder >>
      test_folder: << your/test/folder >>
    commands:
      - phpunit --coverage-clover=coverage.xml --coverage-filter $${source_folder} $${test_folder}
      - sonar-scanner -Dsonar.host.url=$${sonar_host} -Dsonar.login=$${sonar_token} -Dsonar.php.coverage.reportPaths="coverage.xml" -Dsonar.projectKey=$${project_key} -Dsonar.coverage.exclusions="$${test_folder}/**"
```