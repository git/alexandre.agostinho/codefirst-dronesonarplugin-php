FROM aosapps/drone-sonar-plugin AS base

FROM jitesoft/phpunit:8.3

COPY --from=base /bin/drone-sonar /bin/
COPY --from=base /bin/sonar-scanner-4.4.0.2170 /bin/sonar-scanner-4.4.0.2170
ENV PATH ${PATH}:/bin/sonar-scanner-4.4.0.2170/bin

RUN apk update && apk add openjdk11-jre nodejs && rm -rf /tmp/* /var/cache/apk/*
ENV JAVA_HOME /usr/lib/jvm/default-jvm/
ENV PATH ${PATH}:${JAVA_HOME}/bin

WORKDIR /app

RUN php -v

ENTRYPOINT /bin/drone-sonar
